import requests
from requests import Response


class Requests:
    """
    Дополнил класс requests. Необходимый заголовок с авторизацией подставляется сразу.
    """
    headers = dict()

    def get(self, url: str, headers: dict = None, data: dict = None) -> Response:
        """
        Обертка над GET запросом.
        :param headers: Закголовок.
        :param url: адрес, на который отправится запрос.
        :param data: параметры для GET запроса.
        :return: ответ `Response`
        """
        data = data or {}
        headers = headers or self.headers
        result = requests.get(url=url, headers=headers, params=data)
        return result

    def post(self, url: str, data: dict = None, headers: dict = None) -> Response:
        """
        Обертка над POST запросом
        :param headers: Заголовки. По умолчанию используются заголовки, которые заданы в экземпляре класса
        :param url: адрес, на который отправится запрос
        :param data: словарь с данными
        :return: объект `Response`
        """

        data = data or {}
        headers = headers or self.headers
        result = requests.post(url=url, data=data, headers=headers)
        return result
