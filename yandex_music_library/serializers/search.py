import typing
from pydantic import BaseModel, Field, validator

from yandex_music_library.serializers.album import Album
from yandex_music_library.serializers.artist import Artist
from yandex_music_library.serializers.playlist import PlayList
from yandex_music_library.serializers.track import Track


class Search(BaseModel):
    """
    Сериализация результатов поиска
    """

    text: str = Field(description="Поисковый запрос")
    misspell_corrected: bool = Field(alias="misspellCorrected", description="Был ли исправлен поисковый запрос")
    no_correct: bool = Field(alias="nocorrect", description="Если False, то запрос будет исправлен, иначе нет.")
    misspell_result: str = Field(alias="misspellResult", description="Исправленные поисковый запрос")
    misspell_original: str = Field(alias="misspellOriginal", description="Оригинальный поисковый запрос")

    tracks: typing.List[Track] = Field(description="Список треков")
    artists: typing.List[Artist] = Field(description="Список артистов")
    playlists: typing.List[PlayList] = Field(description="Список плейлистов")
    albums: typing.List[Album] = Field(description="Список альбом")

    #
    @validator("tracks", pre=True)
    def check_tracks(cls, value) -> typing.List[Track]:
        """
        Получение информации о треках.
        :param value: Словарь, содержащий одним из полей список треков.
        :return: Список треков
        """
        return value.get("results")

    @validator("artists", pre=True)
    def check_artists(cls, value) -> typing.List[Artist]:
        """
        Получение информации об артистахх.
        :param value: Словарь, содержащий одним из полей список артистов.
        :return: Список артистов
        """
        return value.get("results")

    @validator("playlists", pre=True)
    def check_playlists(cls, value) -> typing.List[Artist]:
        """
        Получение информации о плейлистах.
        :param value: Словарь, содержащий одним из полей список плейлистов.
        :return: Список плейлистов
        """
        return value.get("results")

    @validator("albums", pre=True)
    def check_albums(cls, value) -> typing.List[Artist]:
        """
        Получение информации об альбомах.
        :param value: Словарь, содержащий одним из полей список альбомов.
        :return: Список альбомов
        """
        return value.get("results")
