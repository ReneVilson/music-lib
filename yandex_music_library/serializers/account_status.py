import typing
from pydantic import BaseModel, Field, validator


class AccountStatus(BaseModel):
    """
    Класс, описывающий статус аккаунта
    """

    uid: str
    login: str
    full_name: str = Field(alias="fullName", description="Полное имя")
    birthday: str = Field(alias="birthday", description="День рождения в формате yyyy-mm-dd")
    registered: str = Field(alias="registeredAt", description="Дата регистрации")
    phone: typing.Union[str, None, list] = Field(alias="passport-phones",
                                                 description="Номер телефона из пасспорта яндекса")

    @validator("phone")
    def processing_phone(cls, obj_list: typing.List[dict]) -> typing.Union[str, None]:
        """
        Обработка телефона.
        :param obj_list: Список объектов, который приходит от яндекса.
        :return: номер телефона.
        """
        for obj in obj_list:
            if "phone" in obj:
                return obj.get("phone")
        return None
