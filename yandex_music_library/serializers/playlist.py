import typing

from pydantic import BaseModel, Field, validator


class PlayList(BaseModel):
    """
    Класс, описывающий плейлист
    """

    playlist_uuid: str = Field(alias="playlistUuid", description="UUID плейлиста")
    available: bool = Field(description="Доступность")
    kind: typing.Union[str, int, None] = Field(description="Тип")
    title: str = Field(description="Название плейлиста")
    track_count: int = Field(alias="trackCount", description="Количество треков")
    visibility: typing.Union[str, None] = Field(description="Видимость")
    created: typing.Union[str, None] = Field(description="Дата создания")
    modified: typing.Union[str, None] = Field(description="Дата изменения")
    duration_ms: typing.Union[int, None] = Field(alias="durationMs", description="Продолжительность в мс")
    is_banner: bool = Field(alias="isBanner", description="Баннер", default=False)
    is_premiere: bool = Field(alias="isPremiere", description="Премьера", default=False)
    custom_cover: typing.Union[bool, None] = Field(alias="cover", description="Используется ли кастомная обложка")
    playlist_image: typing.Union[str, None] = Field(alias="ogImage", description="Премьера")

    @validator("playlist_image", pre=True)
    def check_playlist_image(cls, value) -> str:
        """
        Обработка поля для изображения
        :param value: Ответ от яндекса, который нуждается в обработке
        :return: Кастомная обложка на плейлист
        """
        return cls._check_url(value)

    @validator("custom_cover", pre=True)
    def check_custom_cover(cls, value) -> bool:
        """
        Получение информации об обложке.
        :param value: Словарь, содержащий инфу об обложке.
        :return: Кастомная ли обложка.
        """
        return value.get("custom")

    @staticmethod
    def _check_url(url: str) -> str:
        """
        Проверка ссылки на корректность
        :param url: Сырая ссылка на изображение. заменяем символы '%%' на '200x200'.
        :return: ссылка на изображение
        """

        url = f"https://{url.replace('%%', '200x200')}"
        return url
