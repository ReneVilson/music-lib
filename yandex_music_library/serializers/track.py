import typing

from pydantic import BaseModel, Field, validator
from yandex_music_library.serializers.artist import Artist


class Track(BaseModel):
    """
    Сериализация трека
    """

    track_id: typing.Union[int, str] = Field(alias="id", description="ID трека")
    album_id: typing.Union[None, str] = Field(alias="albums", description="ID альбома")
    title: str = Field(description="Название")
    duration: int = Field(alias="durationMs", description="Продолжительность")
    image: typing.Union[str, None] = Field(alias="ogImage", description="Изображение")
    available: bool = Field(description="Доступность")
    artists: typing.List[Artist] = Field(description="Исполнители")

    @validator("album_id", pre=True)
    def check_album_id(cls, values) -> typing.Union[None, str]:
        """
        Получение ID первого альбома.
        :param values: Инфа об альбоме
        :return: ID альбома
        """

        if values:
            return values[0].get("id")
