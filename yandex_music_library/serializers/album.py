import typing
from pydantic import BaseModel, Field, validator, parse_obj_as

from yandex_music_library.serializers.track import Track


class Album(BaseModel):
    """
    Сериализация альбома
    """

    id: str = Field(description="ID альбома")
    title: str = Field(description="Название")
    type: typing.Union[str, None] = Field(alias="metaType", description="Тип")
    year: int = Field(description="Год")
    genre: str = Field(description="Жанры")
    track_count: int = Field(alias="trackCount", description="Количество треков")
    likes_count: int = Field(alias="likesCount", description="Количество жанров")
    available: bool = Field(description="Доступность")
    tracks: typing.Union[typing.List[Track], None] = Field(alias="volumes", description="Треки")

    @validator("tracks", pre=True)
    def check_tracks(cls, values) -> typing.List[Track]:
        """
        Сериализация треков
        :param values: список, который содержит список, который содержит словарь.
        :return: Список треков
        """
        return parse_obj_as(typing.List[Track], values[0])
