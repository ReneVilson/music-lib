from pydantic import BaseModel, Field


class Artist(BaseModel):
    """
    Сериализация объекта артист
    """

    id: int = Field(description="ID артиста")
    name: str = Field(description="Имя")
    composer: bool = Field(description="Автор")
