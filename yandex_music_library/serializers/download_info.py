import requests
from hashlib import md5
from typing import Any
from xml.dom import minidom

from pydantic import BaseModel
from pydantic import Field


class DownloadInfo(BaseModel):
    """
    Сериалайзер для подробной информации о загрузке
    """

    codec: str
    gain: bool
    preview: bool
    download_info_url: str = Field(alias="downloadInfoUrl", description="Ссылка на XML документ")
    direct: bool
    bitrateInKbps: int
    download_link: str

    def __init__(self, **data: Any):
        download_link = self._make_url(data.get("downloadInfoUrl"))
        data["download_link"] = download_link
        super().__init__(**data)

    def _make_url(self, xml_link: str) -> str:
        """
        Построение ссылки для скачивания.
        :param xml_link: Ссылка на xml документ.
        :return: Ссылка на файл.
        """

        result = requests.get(xml_link)

        doc = minidom.parseString(result.text)
        host = self._parse_xml(doc.getElementsByTagName("host"))
        path = self._parse_xml(doc.getElementsByTagName("path"))
        ts = self._parse_xml(doc.getElementsByTagName("ts"))
        s = self._parse_xml(doc.getElementsByTagName("s"))
        sign = md5(("XGRlBW9FXlekgbPrRHuSiA" + path[1::] + s).encode("utf-8")).hexdigest()

        return f"https://{host}/get-mp3/{sign}/{ts}{path}"

    @staticmethod
    def _parse_xml(elements: list) -> str:
        """
        Парсинг xml документа и формирование ссылки на скачивание.

        :param elements: Список узлов в xml документе.
        :return: Элемент который находится в узле xml документа.
        """
        for element in elements:
            nodes = element.childNodes
            for node in nodes:
                if node.nodeType == node.TEXT_NODE:
                    return node.data
