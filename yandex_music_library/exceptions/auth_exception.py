class AuthenticatedFailed(Exception):
    """
    Исключение, если пользователь не совершил вход.
    """

    def __init__(self, message: str = "Пользователь не совершил вход"):
        super(AuthenticatedFailed, self).__init__(message)
