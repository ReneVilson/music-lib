import typing

from pydantic import parse_obj_as
from requests import Response

from yandex_music_library.serializers.account_status import AccountStatus
from yandex_music_library.serializers.album import Album
from yandex_music_library.tools.custom_requests import Requests
from yandex_music_library.serializers.download_info import DownloadInfo
from yandex_music_library.serializers.playlist import PlayList
from yandex_music_library.serializers.search import Search
from yandex_music_library.serializers.track import Track
from yandex_music_library.exceptions import AuthenticatedFailed


class MusicApi(Requests):
    base_url = "https://api.music.yandex.net"
    auth_url = "https://oauth.yandex.ru"

    def __init__(self, client_id: str = "23cabbbdc6cd418abb4b39c32c41195d",
                 client_secret: str = "53bc75238f0c4d08a118e51fe9203300") -> None:
        """
        Класс для работы с апи yandex music
        :param client_id: ID приложения. Выдернуто из официального приложения яндекса
        :param client_secret: секретный ключ. Выдернуто из официального приложения яндекса
        """
        self.client_id = client_id
        self.client_secret = client_secret

        self.user_id = None

        self.token = None
        self.headers = {}

        self.playlists = list()

    def credentials_init(self, username: str, password: str) -> str:
        """
        Вход по логину и паролю
        :param username: Имя пользователя в яндекс музыке
        :param password: Пароль от аккаунта
        :return: Возвращает access_token, с помощью которого можно получить доступ к сервисам яндекса
        """

        data = {
            'grant_type': "password",
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'username': username,
            'password': password
        }

        result = self.post(url=f"{self.auth_url}/token", data=data, check_auth=False)
        self.token = result.json().get("access_token")
        self.user_id = result.json().get("uid")
        self.headers["Authorization"] = f"OAuth {self.token}"
        return self.token

    def token_init(self, token: str) -> None:
        """
        Вход по токену
        :param token: Токен для получения доступа к сервисам яндекса
        :return: None
        """
        self.headers = {"Authorization": f"OAuth {token}"}
        self.account_status()

    def account_status(self) -> AccountStatus:
        """
        Получение пользовательских настроек
        :return: Статус овтета
        """

        url = f"{self.base_url}/account/status"
        result = self.get(url=url, check_auth=False)
        status = AccountStatus(**result.json().get("result", {}).get("account", {}))
        self.user_id = status.uid
        return status

    def get_playlists(self) -> typing.List[PlayList]:
        """
        Получение списка плейлистов
        :return: список плейлистов
        """

        url = f'{self.base_url}/users/{self.user_id}/playlists/list'
        playlist = self.get(url=url)
        return parse_obj_as(typing.List[PlayList], playlist.json().get("result"))

    def get_playlist_tracks(self, kind: int, user_id: int = None) -> typing.List[Track]:
        """
        Получение тревов конкретного пллейлиста.
        :param kind: Идентификатор для плейлиста. Обязательный параметр. Можно узнать, получив все плейлисты.
        :param user_id: Id пользователя. Если не передавать, то автоматически позставится Id авторизированного
        пользователя.

        :return: Объект класса PlayList
        """

        user_id = user_id or self.user_id
        url = f"{self.base_url}/users/{user_id}/playlists"
        response = self.post(url, {"kinds": kind})
        tracks: list = response.json().get("result")[0].get("tracks")
        track_album_pair = [f"{track.get('id')}:{track.get('albumId')}" for track in tracks]
        return self._get_full_track_info(track_id=track_album_pair)

    def get_albums(self, album_ids: typing.Union[str, typing.List[str]]) -> typing.List[Album]:
        """
        Получение информации об альбоме/альбомах.
        :return: Список объектов типа Album
        """
        url = f'{self.base_url}/albums'
        data = {
            f"album-ids": album_ids
        }
        results = self.post(url=url, data=data).json()
        return parse_obj_as(typing.List[Album], results.get("result"))

    def get_albums_track(self, album_id: str) -> Album:
        """
        Получение альбома вместе с треками.
        :param album_id: Идентификатор альбома.
        :return: Объект типа Album.
        """

        url = f"{self.base_url}/albums/{album_id}/with-tracks"
        result = self.get(url).json()
        return Album(**result.get("result"))

    def download_track(self, track_id: typing.Union[int, str]) -> typing.List[DownloadInfo]:
        """
        Получение информации о загрузке
        :param track_id: Идентификатор трека.
        :return:
        """

        url = f"{self.base_url}/tracks/{track_id}/download-info"
        results = self.get(url).json()
        return parse_obj_as(typing.List[DownloadInfo], results.get("result"))

    def search(self, text: str, section: str = "all", no_correct: bool = False, page: int = 0,
               in_the_best: bool = False):
        """
        Поиск по ключевой фразе.
        :param page: Номер страницы.
        :param in_the_best: Осуществить поиск по 'лучшим'.
        :param text: Ключевая фраза, по которой будет осуществляться поиск.
        :param section: Раздел, в котором будет происходить поиск. Варианты: Везде, Плейлисты, Треки, Исполнители.
        :param no_correct: Включено ли исправление орфографии. По дефолту отключено.
        :return: Список
        """

        data = {
            "text": text,
            "nocorrect": no_correct,
            "type": section,
            "page": 0,
            "playlist-in-best": False,
        }

        url = f"{self.base_url}/search"

        result = self.get(url=url, data=data)
        return parse_obj_as(Search, result.json().get("result"))

    def create_play_list(self, title: str, visibility: str = "private") -> PlayList:
        """
        Создание плейлиста для текущего пользователя.
        :param title: Название плейлиста
        :param visibility: Видимость плейлиста. На вход подается строка! public, private
        :return: Объект типа playlist
        """

        url = f"{self.base_url}/users/{self.user_id}/playlists/create"

        data = {
            "title": title,
            "visibility": visibility
        }

        results = self.post(url=url, data=data)
        return parse_obj_as(PlayList, results.json().get("result"))

    def delete_play_list(self, kind: int) -> bool:
        """
        Удаление плейлиста пользователя.
        :param kind: Уникальный идентификатор плейлиста.
        :return: При успехе возвращается True, иначе False.
        """

        url = f"{self.base_url}/users/{self.user_id}/playlists/{kind}/delete"
        results = self.post(url=url)
        return results.status_code == 200

    def rename_play_list(self, kind: int, new_name: str) -> bool:
        """
        Переименование пользовательского плейлиста.
        :param kind: Уникальный идентификатор плейлиста.
        :param new_name: Новое имя плейлиста.
        :return: True если удалось переименовать плейлист, False в противном случае.
        """

        url = f"{self.base_url}/users/{self.user_id}/playlists/{kind}/name"

        data = {
            "value": new_name,
        }
        result = self.post(url=url, data=data)
        return result.status_code == 200

    def _get_full_track_info(self, track_id: typing.Union[str, typing.List[str]]) -> typing.List[Track]:
        """
        Метод для получения подробной информации о треке/треках
        :param track_id: передавать либо строку в формате track_id:album_id либо список строк в том же формате.
        :return:
        """

        url = f'{self.base_url}/tracks'
        data = {
            f"track-ids": track_id
        }
        results = self.post(url=url, data=data)
        return parse_obj_as(typing.List[Track], results.json().get("result"))

    def get(self, url: str, headers: dict = None, data: dict = None, check_auth: bool = True) -> Response:
        """
        Переопределение метода GET. Проверка, получил ли пользователь user_id. Если нет, то пользователь имеет не
        валтдный токен.
        """

        data = data or {}
        if not self.user_id and check_auth:
            raise AuthenticatedFailed()
        return super().get(url=url, headers=headers, data=data)

    def post(self, url: str, data: dict = None, headers: dict = None, check_auth: bool = True) -> Response:
        """
        Переопределение метода POST. Проверка, получил ли пользователь user_id. Если нет, то пользователь имеет не
        валтдный токен.
        """

        if not self.user_id and check_auth:
            raise AuthenticatedFailed()
        return super().post(url, data, headers)
