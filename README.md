# Обертка для апи с музыкой


### Идея для проекта была подсмотрена здесь: https://pypi.org/project/yandex-music/
### Отсюда были получены client_id и client_secret (Вытащены из windows приложения)
### Telegram канал сообщества: https://t.me/yandex_music_api

## Для сериализации и валидации используется pydantic

## Реализовано на данный момент:
- [x] Вход с помощью логина и пароля
- [x] Аутентификация пользователя по токену
- [x] Получение статуса аккаунта
- [x] Получение списка плейлиста/плейлистов
- [x] Получение списка треков у плейлиста
- [x] Получение альбома/альбомов
- [x] Получение подробной информации о треке/треках
- [x] Получение ссылки на загрузку файла (Несколько ссылок с разным кодеком и битрейном)
- [x] Поиск
- [x] Создание плейлиста
- [x] Удаление плейлиста 
- [x] Переименование плейлиста
- [ ] Добавление песен в плейлист
- [ ] Удаление песен из плейлиста