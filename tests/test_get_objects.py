import unittest

from yandex_music_library import MusicApi
from yandex_music_library.serializers.playlist import PlayList
from yandex_music_library.serializers.track import Track


class TestGetObjects(unittest.TestCase):
    """
    Тестирование получения объектов
    """

    def setUp(self) -> None:
        self.token = "AgAAAAAPU-xBAAG8XhecKtY8tErVtfh2yc621NI"
        self.music = MusicApi()
        self.music.token_init(token=self.token)

    def test_is_playlist_list(self):
        playlists = self.music.get_playlists()
        self.assertIsInstance(playlists, list)
        for playlist in playlists:
            self.assertIsInstance(playlist, PlayList)

    def test_is_tracks_instance(self):
        tracks = self.music.get_playlist_tracks(1001)
        self.assertIsInstance(tracks, list)
        for track in tracks:
            self.assertIsInstance(track, Track)


if __name__ == '__main__':
    unittest.main()
