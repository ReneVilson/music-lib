import unittest

from yandex_music_library import MusicApi


class TestAuthMethods(unittest.TestCase):

    def setUp(self) -> None:
        self.email = "rinat0010@yandex.ru"
        self.password = "nerod&423818"
        self.music = MusicApi()
        self.token = "AgAAAAAPU-xBAAG8XhecKtY8tErVtfh2yc621NI"

    def test_credentials_init(self):
        """
        Тест входа по логину и паролю
        """
        resp = self.music.credentials_init(self.email, self.password)
        self.assertIsInstance(resp, str)
        self.token = resp

    def test_token_init(self):
        """
        Тест входа по токену
        """
        self.music.token_init(self.token)

        self.assertIsNotNone(self.music.user_id, f"token: {self.token}")


if __name__ == '__main__':
    unittest.main()
